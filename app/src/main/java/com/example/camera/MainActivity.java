package com.example.camera;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.ByteArrayOutputStream;

public class MainActivity extends AppCompatActivity {

    private Button btnOpenCamera, btnSubmit;
    private ImageButton btnRotate;
    private ImageView imvAvatar;
    private EditText txtID, txtEmail, txtPhone;
    private Bitmap photo = null;

    // Rotate bitmap image
    private Bitmap rotateBitmap(Bitmap source, int degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(
                source, 0, 0, source.getWidth(), source.getHeight(), matrix, true
        );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.btnOpenCamera = findViewById(R.id.btnOpenCamera);
        this.btnSubmit = findViewById(R.id.btnSubmit);
        this.btnRotate = findViewById(R.id.btnRotate);

        this.imvAvatar = findViewById(R.id.imvAvatar);

        this.txtID = findViewById(R.id.txtID);
        this.txtEmail = findViewById(R.id.txtEmail);
        this.txtPhone = findViewById(R.id.txtPhone);

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.CAMERA}, 101);
        }

        // OnClick for btnOpenCamera
        btnOpenCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 101);
            }
        });

        // OnClick for btnRotate
        btnRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (photo != null) {
                    int w = photo.getWidth();
                    int h = photo.getHeight();
                    Bitmap bmp = photo.copy(photo.getConfig(), true);
                    Bitmap rotatedImage = rotateBitmap(bmp, 90);

                    imvAvatar.setImageBitmap(rotatedImage);
                    photo = rotatedImage.copy(photo.getConfig(), false);
                } else {
                    Toast.makeText(getApplicationContext(),"Please Choose Image !!!", Toast.LENGTH_LONG).show();
                }
            }
        });

        // OnClick for btnSubmit
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strID = txtID.getText().toString();
                String strPhone = txtPhone.getText().toString();
                String strEmail = txtEmail.getText().toString();

                if (strID.isEmpty()) {
                    Toast.makeText(getApplicationContext(),"Please Input ID !!!", Toast.LENGTH_LONG).show();
                    txtID.requestFocus();
                    return;
                }

                if (strPhone.isEmpty()) {
                    Toast.makeText(getApplicationContext(),"Please Input Phone !!!", Toast.LENGTH_LONG).show();
                    txtPhone.requestFocus();
                    return;
                }

                if (strEmail.isEmpty()) {
                    Toast.makeText(getApplicationContext(),"Please Input Email !!!", Toast.LENGTH_LONG).show();
                    txtEmail.requestFocus();
                    return;
                }

                if (photo == null) {
                    Toast.makeText(getApplicationContext(),"Please Choose Avatar Image !!!", Toast.LENGTH_LONG).show();
                    return;
                }

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                Bitmap bmp=photo.copy(photo.getConfig(),true);
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                Intent intent = new Intent(MainActivity.this, ActivityShowInfo.class);
                intent.putExtra("id", strID);
                intent.putExtra("phone", strPhone);
                intent.putExtra("email", strEmail);
                intent.putExtra("photo", byteArray);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            photo = (Bitmap) data.getExtras().get("data");
            imvAvatar.setImageBitmap(photo);
        }
    }
}