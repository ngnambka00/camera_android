package com.example.camera;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;

public class ActivityShowInfo extends AppCompatActivity {
    private EditText txtID, txtPhone, txtEmail;
    private ImageView imvAvatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_info);
        this.txtID = findViewById(R.id.txtID);
        this.txtPhone = findViewById(R.id.txtPhone);
        this.txtEmail = findViewById(R.id.txtEmail);
        this.imvAvatar = findViewById(R.id.imvAvatar);

        Intent intent = getIntent();
        byte[] byteArray = intent.getByteArrayExtra("photo");
        String strID = intent.getStringExtra("id");
        String strPhone = intent.getStringExtra("phone");
        String strEmail = intent.getStringExtra("email");

        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        imvAvatar.setImageBitmap(bmp);

        txtID.setText(strID);
        txtPhone.setText(strPhone);
        txtEmail.setText(strEmail);
    }
}